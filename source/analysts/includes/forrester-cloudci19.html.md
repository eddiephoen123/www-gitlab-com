---
layout: markdown_page
title: 'The Forrester Wave™: Cloud-Native Continuous Integration Tools, 2019'
---
## GitLab and The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019

This page represents how Forrester views our cloud-native CI tools in relation to the larger market and how we're applying that information as part of our ongoing product evolution.

![Forrester CI Wave](images/home/forrester-cloud-ci-wave-graphic.png){: .small}

### Forrester's Key Takeaways on the Cloud-Native CI Tools Market at time of report publication:

**Google, GitLab, Microsoft, AWS, And CircleCI Lead The Pack**
Forrester's research uncovered a market in which Google, GitLab, Microsoft, AWS, and CircleCI are Leaders; CloudBees and Atlassian are Strong Performers; Buildkite and Codefresh are Contenders; and Travis CI is a Challenger.

**Speed, Scale, And Security Are The Important Differentiators**
As organizations transition to continuous delivery (CD) and shift hosting of production workloads to cloud servers, traditional, on-premises continuous integration will no longer suffice. Cloud-native CI products with exceptional build speed, on-demand scale, and secure configurations will lead the market and enable customers to accelerate delivery speed and lower management costs, all while meeting corporate compliance needs.

### Forrester's take on GitLab at time of report publication:

GitLab's simple and cohesive approach lands it squarely as a leader. GitLab's approach of having a single application to manage each phase of software development comes through in its developer experience. The UI is easy to use, cohesive, and comes integrated with collaborative tools and development tasks at a glance; it was one of the most fluid UIs we evaluated. GitLab also shines with its preconfigured environments and its ability to support on-premises build agents that can execute on multiple operating systems, including Linux, Mac, and Windows.

GitLab's continuous delivery capability includes support for container registries and Kubernetes, making it easy to get started. GitLab's investment in value stream management also pays off with strong analytics, and customers liked the flexibility of being able to orchestrate builds in the cloud but run them on-premises if they want to. Reference customers would like improved customer support as well as support for greater rules complexity for managing code branches.



